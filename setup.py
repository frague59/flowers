from distutils.core import setup

setup(
    name='flowers',
    version='0.1.0',
    packages=['flowers'],
    url='https://gitlab.com/frague59/flowers',
    license='MIT',
    author='François GUÉRIN',
    author_email='frague59@gmail.com',
    description='Generative graphics'
)
