import sys
import logging
import click
from . import FlowerGenerator

logger = logging.getLogger(__name__)


@click.command()
def start():
    gen = FlowerGenerator()  # noqa
    gen.draw(100)
    gen.mainloop()
    return 0


if __name__ == "__main__":
    sys.exit(start())
