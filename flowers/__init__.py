import logging
import math
import random
from time import sleep
from tkinter import Tk, Canvas  # noqa
from tkinter.ttk import *  # noqa

import lorem

from .helpers import Oscillator, FONTS, Font, COLORS

logger = logging.getLogger(__name__)

WIDTH = 1200
HEIGHT = 1024


class FlowerGenerator(Tk):
    canvas: Canvas = None  # noqa

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title("Yolo")
        self.canvas = Canvas(self, width=WIDTH, height=HEIGHT, background="white")
        self.canvas.pack()

    def draw(self, count: int = 100):
        for _ in range(count):
            pos_x = random.randint(0, WIDTH)
            self._draw_text_as_grass(
                text=lorem.sentence().strip(".").strip(),
                pos_x=pos_x,
                step=random.randint(5, 10) / 100,
                color=random.choice(COLORS),
                font=Font(family=random.choice(FONTS), size=random.randint(20, 50)),
                oscillator=Oscillator(freq=random.randint(20, 120), amplitude=random.randint(15, 30)),
                interval=random.randint(0, 100) / 10_000,
            )

    def _draw_text_as_grass(
            self,
            text: str,
            pos_x: int,
            step: float = 0.025,
            color: str = "darkblue",
            font: Font = Font(family="DejaVu", size=40),
            oscillator: Oscillator = Oscillator(freq=50, amplitude=30),
            interval: float = 0.0025,
    ):
        _pos_x = pos_x
        _pos_y = round(HEIGHT - font.size)
        _font = font
        for char in reversed(text):
            self._draw_char(text=char, pos_x=_pos_x, pos_y=_pos_y, fill=color, font=str(_font), interval=interval)
            _font.size = round(_font.size / (1 + step))
            _pos_y -= round(_font.size + 5) * 1.1
            _pos_x = round(math.sin(_pos_y / oscillator.freq) * oscillator.amplitude / (1 + step)) + pos_x

    def _draw_char(self, text, pos_x, pos_y, fill, font, interval):
        self.canvas.create_text(pos_x, pos_y, fill=fill, font=font, text=text)
        self.canvas.update()
        if interval > 0.0:
            sleep(interval)

