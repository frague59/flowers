"""
helpers for :mod:`flowers` application

:creationdate: 17/12/2020 10:24
:moduleauthor: François GUÉRIN <frague59@gmail.com>
:modulename: flowers.helpers

"""
import logging
from dataclasses import dataclass

__author__ = 'fguerin'
logger = logging.getLogger('flowers.helpers')


COLORS = (
    "darkblue",
    "darkred",
    "darkorange3",
    "black",
    "red",
    "blue4",
    "dodgerblue4",
    "sienna3",
    "green",
    "purple",
    "yellow",
)

FONTS = (
    "DejaVue",
    "Courier",
    "Times",
    # "Helvetica",
    "Crystal",
)


@dataclass
class Font:
    family: str
    size: int

    def __str__(self):
        return f"{self.family} {self.size}"


@dataclass
class Oscillator:
    freq: int
    amplitude: int
